from dolfin import *
import numpy as np
import matplotlib.pyplot as plt


set_log_active(False)

# Define Dirichlet boundary (x = 0 or x = 1)
def p_boundary(x):
    return x[0] > 1.0 - DOLFIN_EPS
    

def modelling_error_p():
    # Create mesh and define function space
    n = 128

    mesh = UnitSquareMesh(n, n)

    Q = FunctionSpace(mesh, "Lagrange", 1)
    DG0 = FunctionSpace(mesh, 'DG',0)

    # Define boundary conditions and IC
    p_ = Constant(0.0)                    # primal p
    p_0 = Constant(0.0)                   # coarse p0
    w_ = Constant(0.0)
    w_0 = Constant(0.0)

    bc_p = DirichletBC(Q, p_, p_boundary)         # primal p
    bc_p0 = DirichletBC(Q, p_0, p_boundary)       # coarse p0
    bc_w = DirichletBC(Q, w_, p_boundary)         # primal dual w
    bc_w0 = DirichletBC(Q, w_0, p_boundary)       # coarse dual w0


    # Define variational problem
    p = TrialFunction(Q)                        # primal p
    p0 = TrialFunction(Q)                       # coarse p0
    w = TrialFunction(Q)                        # primal dual w
    w0 = TrialFunction(Q)                       # coarse dual w
    q = TestFunction(Q)
    g = Constant(-1.0)
    g1 = Expression('-x[1]', degree=1)

    K = Function(DG0)
    n = DG0.dim()


    for i in range(n):
        np.random.seed(n)
        K.vector()[i] = 1.0 +  0.5*np.random.rand(n)[i]

    # Compute e.g. average to define K0
    K0 = Constant(1.0/n*sum(K.vector().array())) 

    # solve coarse problem 
    a_p0 = inner(K0*grad(p0),grad(q))*dx 
    L = inner(g,q)*dx 

    # solve primal problem 
    a_p = inner(K*grad(p),grad(q))*dx

    # solve dual coarse problem
    a_w0 = inner(K0*grad(w0),grad(q))*dx
    L_w = inner(g1,q)*dx

    # solve dual primal problem 
    a_w = inner(K*grad(w),grad(q))*dx

    # Define function for the solutions
    p = Function(Q) 
    p0 = Function(Q) 
    w = Function(Q)
    w0 = Function(Q)

    solve(a_w0 == L_w, w0, bc_w0)
    solve(a_p0 == L, p0, bc_p0)
    solve(a_p == L, p, bc_p)
    solve(a_w == L_w, w, bc_w)
    def KI1_grad(q):
        return (K-K0)*grad(q)

    def I1_grad(q):
        return (1 - 1./K *K0)*grad(q)    

    def residual(q,p0):
        return assemble((-inner(grad(q),KI1_grad(p0))*dx))

    def ksi_upp(q):
        return float(np.sqrt(assemble(inner(I1_grad(q),K*(1-1./K*K0)*grad(q))*dx)))

    def ksi_low(q):
        return float((np.abs(residual(q,q))/norm(q)))

    def norm(q):
        return float(np.sqrt(assemble((inner(grad(q),K*grad(q))*dx))))

    def S_p():
        return float(np.sqrt(ksi_upp(w0)/ksi_upp(p0)))

    def gamma_upp_plus():
        mellom = (assemble(inner(I1_grad(w0),KI1_grad(p0))*dx))
        return float(np.sqrt((S_p()**2 * ksi_upp(p0)**2 + 2.*mellom + S_p()**(-2.)*ksi_upp(w0)**2)))

    def gamma_upp_minus():
        mellom = (assemble(inner(I1_grad(p0),KI1_grad(w0))*dx))
        return float(np.sqrt(S_p()**2 * ksi_upp(p0)**2 - 2.*mellom + S_p()**(-2.)*ksi_upp(w0)**2))

    def gamma_est_upp(): 
        return 1./4.*(gamma_upp_plus()**2) - 1./4.*(gamma_upp_minus()**2) + residual(w0,p0)

    def theta_plus():
        A_p_w = assemble(inner(K*grad(p0), grad(w0))*dx)
        A_p_p = assemble(inner(K*grad(p0), grad(p0))*dx)
        A_w_w = assemble(inner(K*grad(w0), grad(w0))*dx)

        res_p = assemble((-inner((S_p()*grad(p0) + 1./S_p()*grad(w0)), KI1_grad(p0)) )*dx)
        res_w = assemble((-inner((S_p()*grad(p0) + 1./S_p()*grad(w0)), KI1_grad(w0)) )*dx)
        return (A_p_w*res_p - A_p_p*res_w)/(A_p_w*res_w - A_w_w*res_p)

    def theta_minus():
        A_p_w = assemble(inner(K*grad(p0), grad(w0))*dx)
        A_p_p = assemble(inner(K*grad(p0), grad(p0))*dx)
        A_w_w = assemble(inner(K*grad(w0), grad(w0))*dx)
        dabla = assemble(inner(float(S_p())*grad(p0), grad(p0))*dx)
        res_p = assemble((-inner((S_p()*grad(p0) - 1./S_p()*grad(w0)), KI1_grad(p0)) )*dx)
        res_w = assemble((-inner((S_p()*grad(p0) - 1./S_p()*grad(w0)), KI1_grad(w0)) )*dx)
        return (A_p_w*res_p - A_p_p*res_w)/(A_p_w*res_w - A_w_w*res_p)

    def gamma_low_plus():
        res_p_w = assemble(-inner((grad(p0) + theta_plus()*grad(w0)), (S_p()*KI1_grad(p0) + 1./S_p()*KI1_grad(w0)  ) )*dx )  
        norm_p_w = assemble((inner((grad(p0) + theta_plus()*grad(w0)), K*grad(p0) + theta_plus()*K*grad(w0)))*dx)
        gamma_low_plus = float(np.abs(res_p_w)/(norm_p_w))
        return gamma_low_plus

    def gamma_low_minus():
        res_p_w = assemble(-inner((grad(p0) + theta_minus()*grad(w0)), (S_p()*KI1_grad(p0) + 1./S_p()*KI1_grad(w0)  ) )*dx )    
        norm_p_w = assemble((inner((grad(p0) + theta_minus()*grad(w0)), K*grad(p0) + theta_minus()*K*grad(w0)))*dx)
        gamma_low_minus = float(np.abs(res_p_w)/(norm_p_w))
        return gamma_low_minus

    def gamma_est_low():
        return 1./4.*(gamma_low_plus()**2) - 1./4.*(gamma_low_minus()**2) + residual(w0,p0)

    def gamma_low():
        return 1./4*(gamma_low_minus()**2) -1./4*(gamma_upp_minus()**2) + residual(w0,p0) 

    def gamma_upp():
        return  1./4*(gamma_upp_plus()**2) -1./4*(gamma_low_minus()**2) + residual(w0,p0)  


    residual_ = residual(w0,p0)
    ksi_upp_p = ksi_upp(p0)
    ksi_low_p = ksi_low(p0)
    ksi_upp_w = ksi_upp(w0)
    ksi_low_w = ksi_low(w0)
    error_p = norm(p-p0)
    error_w = norm(w-w0)
    p_effectivity_ind_upp = ksi_upp_p/error_p
    p_effectivity_ind_low = ksi_low_p/error_p
    w_effectivity_ind_upp = ksi_upp_w/error_w
    w_effectivity_ind_low = ksi_low_w/error_w
    gamma_est_upp_ = gamma_est_upp()
    gamma_est_low_ = gamma_est_low()
    gamma_low_ = gamma_low()
    gamma_upp_ = gamma_upp()
    QOI_effectivity_ind_low = gamma_low_/gamma_est_upp_ 
    QOI_effectivity_ind_upp = gamma_upp_/gamma_est_upp_ 
    gamma_low_minus_ = gamma_low_minus()
    gamma_low_plus_ = gamma_low_plus()
    gamma_upp_minus_ = gamma_upp_minus()
    gamma_upp_plus_ = gamma_upp_plus()

    if ksi_low_p<= error_p and error_p <= ksi_upp_p:
        print ("------------------------------------------------------------")
        print ("ksi_low_p = %.2e, error_p = %.2e, ksi_upp_p = %.2e" %(ksi_low_p, error_p, ksi_upp_p))
        print ('p_effectivity_ind_low = %.2e, p_effectivity_ind_upp = %.2e' % (p_effectivity_ind_low,p_effectivity_ind_upp))
    else:
        print ('Test is not pass')

    if ksi_low_w<= error_w and error_w <= ksi_upp_w:
        print ("------------------------------------------------------------")
        print ("ksi_low_w = %.2e, error_w = %.2e, ksi_upp_w = %.2e" %(ksi_low_w, error_w, ksi_upp_w))
        print ('w_effectivity_ind_low = %.2e, w_effectivity_ind_upp = %.2e' % (w_effectivity_ind_low,w_effectivity_ind_upp))
        print ("------------------------------------------------------------")
    else:
        print ('Test is not pass')

    ##print gamma_est_upp_, gamma_est_low_, gamma_est_upp_
    if gamma_low_ <= gamma_est_upp_ and gamma_est_upp_ <= gamma_upp_ :
        print ('gamma_low = %.2e, L(e) = %.2e, gamma_upp = %.2e' % (gamma_low_,gamma_est_upp_,gamma_upp_))
        print ('QOI_effectivity_ind_low = %.2e, QOI_effectivity_ind_upp = %.2e' % (QOI_effectivity_ind_low, QOI_effectivity_ind_upp))
        print ("------------------------------------------------------------")
    else:
        print ('Test is not pass')

    ufile = File('res/p_0.pvd')
    ufile << p0
    ufile = File('res/w_0.pvd')
    ufile << w0
    file = File("res/K.pvd")
    file << K
    file = File("res/w.pvd")
    file << w
    file = File("res/p.pvd")
    file << p

    return p, p0, w, w0, K, K0, ksi_upp_w, ksi_low_w, ksi_upp_p, ksi_low_p, gamma_low_minus_, gamma_low_plus_, gamma_upp_minus_, gamma_upp_plus_, mesh, n, error_p 

modelling_error_p()