from dolfin import *
import numpy as np
import sympy as sym
from sympy.utilities.codegen import ccode
import matplotlib.pyplot as plt


set_log_active(False)



class Left(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] < DOLFIN_EPS
        
class Right(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] > 1.0 - DOLFIN_EPS
        
class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return x[1] < DOLFIN_EPS
        
class Top(SubDomain):
    def inside(self, x, on_boundary):
        return x[1] > 1.0 - DOLFIN_EPS



def solver(Nx, Ny, T, dt, u_exact, p_exact, g, f,  alpha, c, K, mu, lamb):

    mesh = UnitSquareMesh(Nx,Ny)

    V = VectorElement("CG", triangle, 2)
    Q = FiniteElement("CG", triangle, 1)
    W = MixedElement([V,Q])                 # mixed function space

    W = FunctionSpace(mesh, W)

    # Define variational problem
    up = TrialFunction(W)
    vq = TestFunction(W)
    u, p = split(up)
    v, q = split(vq)

  
    # Initial functions
    up0 = Function(W)
    u0, p0 = split(up0)
    boundaries = FacetFunction("size_t", mesh)
    boundaries.set_all(1)

    # define boundary for displacement and pressure
    bc_u = DirichletBC(W.sub(0), u_exact, boundaries,1)
    bc_p = DirichletBC(W.sub(1), p_exact, boundaries,1)
    bc = [bc_u, bc_p]

    # Define strain and stress
    def eps(u):
        return 0.5*(grad(u) + grad(u).T)

    def sigma(u):
        return lamb*div(u)*Identity(2) + 2*mu*eps(u)
            
    # Variational problem
    a1 = (inner(sigma(u),eps(v)) - alpha*inner(p,div(v)))*dx 
    L1 = (inner(f,v))*dx
    a2 = (c*inner(p,q) + alpha*inner(div(u),q) + K*dt*inner(grad(p),grad(q)))*dx
    L2 = (dt*inner(g,q) + alpha*inner(div(u0),q))*dx + c*inner(p0,q)*dx 
    a = a1 + a2
    L = L1 + L2
    
    # Numerical solution
    up_ = Function(W)

    A1 = assemble(a)
    [bc_.apply(A1) for bc_ in bc]

    # Solve for each time step t
    t = 0
            
    while t <= T:    
                              
        t += dt  
                      
        # Update source terms
        u_exact.t = t
        p_exact.t = t

        # Update current time
        g.t = t
        f.t = t
        b1 = assemble(L)
        [bc_.apply(b1) for bc_ in bc]
        # Solve variational problem
        solve(A1, up_.vector(), b1) #, 'bicgstab', 'hypre_amg')

        # Update previous solution 
        up0.assign(up_)
         
    # Numerical solutions
    u_, p_ = up_.split()
    return u_, p_,  u_exact, p_exact, mesh

def run_solver():
    # Parameters
    c = Constant(0.0000)         # compressibility Q^-1
    K = Constant(0.00001)            # mobility
    T = 1.0                      # final time
    alpha = Constant(1.0)        # Biot-Willis coefficient
    dt = 1./32

    # Elasticity parameters
    #E, nu = 10.0, 0.3
    #mu, lamb = Constant(E/(2*(1 + nu))), Constant(E*nu/((1 + nu)*(1 - 2*nu)))
    mu = Constant(1.0)
    lamb = Constant(100.0)
    # Source terms



    # -------- Use SymPy to compute f and g from the manufactured solution u and p -------- 
    x, y, t = sym.symbols("x[0] x[1] t")

    
    u_exact_ = [-1/(4*sym.pi) *(sym.cos(2*sym.pi*x)*sym.sin(2*sym.pi*y)*sym.sin(2*sym.pi*t)), -1/(4*sym.pi) *(sym.sin(2*sym.pi*x)*sym.cos(2*sym.pi*y)*sym.sin(2*sym.pi*t))]
    p_exact_ = sym.sin(2*sym.pi*x)*sym.sin(2*sym.pi*y)*sym.sin(2*sym.pi*t) 

    str1 = sym.printing.ccode(u_exact_[0]).replace('M_PI', 'pi')
    str2 = sym.printing.ccode(u_exact_[1]).replace('M_PI', 'pi')
    tupleOfStrings = (str1,str2)

    u_exact = Expression(tupleOfStrings , t=0.0, degree = 3, cell=tetrahedron)
    p_exact = Expression(sym.printing.ccode(p_exact_).replace('M_PI', 'pi'), t=0.0, degree=3, cell=tetrahedron)

    g_ = sym.simplify(c*sym.diff(p_exact_, t) + alpha*( sym.diff(sym.diff(u_exact_[0], t), x) + sym.diff(sym.diff(u_exact_[1], t), y) ) - K*((sym.diff(p_exact_, x, 2) + sym.diff(p_exact_,y,2))))
    f_1 = sym.simplify(- (2*mu * (sym.diff(u_exact_[0], x, 2) + sym.diff(u_exact_[0], x, 2) + sym.diff(u_exact_[1],x,y)) + lamb * (sym.diff(u_exact_[0],x,2) + sym.diff(u_exact_[1],x,y)) - alpha * sym.diff(p_exact_,x) ))
    f_2 = sym.simplify(- (2*mu * (sym.diff(u_exact_[1], y, 2) + sym.diff(u_exact_[0], y, x) + sym.diff(u_exact_[1],y,2)) + lamb * (sym.diff(u_exact_[0],x,y) + sym.diff(u_exact_[1],y,2)) - alpha * sym.diff(p_exact_,y) ))
    
    str3 = sym.printing.ccode(f_1).replace('M_PI', 'pi')
    str4 = sym.printing.ccode(f_2).replace('M_PI', 'pi')
    tupleOfStrings_1 = (str3,str4)


    # Convert to Dolfin expression. Note that args t, c, alpha, lanb, mu are given values
    g = Expression(sym.printing.ccode(g_).replace('M_PI', 'pi'), c=c, alpha=alpha, K=K, t=0.0, degree = 3)
    f = Expression(tupleOfStrings, t=0.0, mu=mu, lamb=lamb, alpha=alpha, degree = 3)

    u_, p_,u_exact, p_exact, mesh = solver(16, 16, T, dt, u_exact, p_exact, g, f,  alpha, c, K, mu, lamb)


    #plot(u_)
    #plot(u_.function_space(), mesh())
    #plot(p_)
    #plot(p_.function_space(), mesh())

    ufile = File('res/u.pvd')
    pfile = File('res/p.pvd')
    ufile << u_
    pfile << p_

   
    #interactive() 
    #plt.show()  

def convergence_rate():
    # Parameters
    c = Constant(0.0)            # compressibility Q^-1
    K = Constant(0.0000001)      # mobility
    T = 1.0                      # final time
    alpha = Constant(1.0)        # Biot-Willis coefficient

    # Elasticity parameters
    #E, nu = 10.0, 0.3
    #mu, lamb = Constant(E/(2*(1 + nu))), Constant(E*nu/((1 + nu)*(1 - 2*nu)))
    mu = Constant(1.0)
    lamb = Constant(100000.0)

    # -------- Use SymPy to compute f and g from the manufactured solution u and p -------- 
    x, y, t = sym.symbols("x[0] x[1] t")

    
    u_exact_ = [-1/(4*sym.pi) *(sym.cos(2*sym.pi*x)*sym.sin(2*sym.pi*y)*sym.sin(2*sym.pi*t)), -1/(4*sym.pi) *(sym.sin(2*sym.pi*x)*sym.cos(2*sym.pi*y)*sym.sin(2*sym.pi*t))]
    p_exact_ = sym.sin(2*sym.pi*x)*sym.sin(2*sym.pi*y)*sym.sin(2*sym.pi*t) 

    str1 = sym.printing.ccode(u_exact_[0]).replace('M_PI', 'pi')
    str2 = sym.printing.ccode(u_exact_[1]).replace('M_PI', 'pi')
    tupleOfStrings = (str1,str2)

    u_exact = Expression(tupleOfStrings , t=0.0, degree = 3, cell=tetrahedron)
    p_exact = Expression(sym.printing.ccode(p_exact_).replace('M_PI', 'pi'), t=0.0, degree=3, cell=tetrahedron)

    g_ = sym.simplify(c*sym.diff(p_exact_, t) + alpha*( sym.diff(sym.diff(u_exact_[0], t), x) + sym.diff(sym.diff(u_exact_[1], t), y) ) - K*((sym.diff(p_exact_, x, 2) + sym.diff(p_exact_,y,2))))
    f_1 = sym.simplify(- (2*mu * (sym.diff(u_exact_[0], x, 2) + sym.diff(u_exact_[0], x, 2) + sym.diff(u_exact_[1],x,y)) + lamb * (sym.diff(u_exact_[0],x,2) + sym.diff(u_exact_[1],x,y)) - alpha * sym.diff(p_exact_,x) ))
    f_2 = sym.simplify(- (2*mu * (sym.diff(u_exact_[1], y, 2) + sym.diff(u_exact_[0], y, x) + sym.diff(u_exact_[1],y,2)) + lamb * (sym.diff(u_exact_[0],x,y) + sym.diff(u_exact_[1],y,2)) - alpha * sym.diff(p_exact_,y) ))
    
    str3 = sym.printing.ccode(f_1).replace('M_PI', 'pi')
    str4 = sym.printing.ccode(f_2).replace('M_PI', 'pi')
    tupleOfStrings_1 = (str3,str4)


    # Convert to Dolfin expression. Note that args t, c, alpha, lanb, mu are given values
    g = Expression(sym.printing.ccode(g_).replace('M_PI', 'pi'), c=c, alpha=alpha, K=K, t=0.0, degree = 3)
    f = Expression(tupleOfStrings, t=0.0, mu=mu, lamb=lamb, alpha=alpha, degree = 3)

    error_u = [];  error_p = []

    tol = 1E-10
    h = []
    for dt in [1./4, 1./8, 1./16, 1./32, 1./64]:
        for N in [4,8,16,32,64]:

            # compute solution
            u_h, p_h, u_analytical, p_analytical, mesh = solver(N, N, T, dt, u_exact, p_exact, g, f,  alpha, c, K, mu, lamb)
           
            error_p_ = errornorm(p_analytical, p_h,  "l2")
            error_u_ = errornorm(u_analytical, u_h,  "l2")
            error_u.append(error_u_); error_p.append(error_p_)

            # Define the element size h = 1/N
            h.append(1./N)
            
            if N == 4:
                print ("dt = %.3f, n = %.d, error_u: %.2e" % (dt, N, error_u_))
                print ("dt = %.3f, n = %.d, error_p: %.2e" % (dt, N, error_p_))


            # --------Convergence rate----------#
            #                                   #
            #        ln(E[i] - E[i-1])          #
            #    r = ------------------         #
            #        ln (h[i] - h[i-1])         #
            #                                   #
            # ----------------------------------#
            
            else:
                convergence_rate_u = np.log(error_u[-2]/error_u[-1])/np.log(h[-2]/h[-1])
                convergence_rate_p = np.log(error_p[-2]/error_p[-1])/np.log(h[-2]/h[-1])

                print ("dt = %.4f, n = %.d, error_u = %.2e, convergence rate = %.3f " % (dt, N, error_u_, convergence_rate_u))
                print ("dt = %.4f, n = %.d, error_p = %.2e, convergence rate = %.3f " % (dt, N, error_p_, convergence_rate_p))
            
    # Error plots
    visualization(h, error_u, error_p)
             
def visualization(h, error_u, error_p) :           
        
    error_u = np.array(error_u); error_p = np.array(error_p)
    plt.loglog(h, error_u, label="u")
    plt.loglog(h, error_p, label="p")
    plt.legend(loc=2)
    plt.ylabel("$\log(error)$", fontsize=20)
    plt.xlabel("$\log(h)$", fontsize=20)
    plt.title("Error in L2 norm")
    plt.savefig("error.png")

    
if __name__ == "__main__":
    #run_solver()
    #interactive()

    convergence_rate()