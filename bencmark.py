from dolfin import *
import numpy as np
import sympy as sym
from sympy.utilities.codegen import ccode
import matplotlib.pyplot as plt


set_log_active(False)

#  Create classes for defining parts of the boundaries and the interior of the domain

class Right(SubDomain):
    def inside(self, x, on_boundary):
        return abs(x[0] - 1.0) < DOLFIN_EPS and on_boundary

class Left(SubDomain):
    def inside(self, x, on_boundary):
        return abs(x[0]) < DOLFIN_EPS and on_boundary

class Top(SubDomain):
    def inside(self, x, on_boundary):
        return abs(x[1] - 1.0) < DOLFIN_EPS and on_boundary

class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return abs(x[1]) < DOLFIN_EPS and on_boundary

class Obstacle(SubDomain):
    def inside(self, x, on_boundary):
        return (between(x[1], (0.25, 0.75)) and between(x[0], (0.0, 1.0)))


def biot():
    
    N = [64]#,8,16,32 ]#,64]
    h = np.zeros(len(N))
    plt.figure()
    for deg in [1,2]: #,3,4]:
        for n in range(len(N)):        

            h[n] = 1./N[n]
            mesh = UnitSquareMesh(N[n],N[n])

            V = VectorElement("CG", mesh.ufl_cell(), deg+1)
            Q = FiniteElement("CG", mesh.ufl_cell(), deg)
            W = FunctionSpace(mesh, V*Q)   


            # Functions
            up = TrialFunction(W)
            vq = TestFunction(W)
            u, p = split(up)
            v, q = split(vq)
                  
            # Initial functions
            up0 = Function(W)
            u0, p0 = split(up0)


            # Parameters
            alpha = Constant(1.0)           # Biot-Willis coefficient
            c = Constant(0.0)           # compressibility
            K_0 = Constant(1E-8)           # mobility
            K = Constant(1.0)           # mobility

            dt = 1.0
            T = 15.0                         # final time

            # Elasticity parameters
            mu = Constant(0.0)
            lamb = Constant(10.0)

            # Source terms
            g = Constant(0.0)
            f = Constant((0.0, 0.0))           

            # Symmetric gradient
            def eps(u):
                return 0.5*(grad(u) + grad(u).T)

            def sigma(u):
                return lamb*div(u)*Identity(2) + 2*mu*eps(u)

                

            # Marking boundary domains
            left    = Left()
            top     = Top()
            right   = Right()
            bottom  = Bottom()
            obstacle = Obstacle()

            domains = CellFunction("size_t", mesh)
            domains.set_all(0)
            obstacle.mark(domains,1)

            boundaries = FacetFunction("size_t", mesh)
            boundaries.set_all(0)
            left.mark(boundaries, 1)
            right.mark(boundaries, 2)
            bottom.mark(boundaries, 3)
            top.mark(boundaries, 4)
            
            # Define Dirichlet boundary conditions 
            bc_u_left = DirichletBC(W.sub(0).sub(0), Constant(0.0), boundaries,1)
            bc_u_right = DirichletBC(W.sub(0).sub(0), Constant(0.0), boundaries,2)
            bc_u_bottom = DirichletBC(W.sub(0).sub(1), Constant(0.0), boundaries,3)
            bc_p_top = DirichletBC(W.sub(1), Constant(0.0), boundaries,4)
            bc = [bc_u_left, bc_u_right, bc_u_bottom, bc_p_top]

            ds = Measure('ds', domain = mesh, subdomain_data = boundaries)
            dx = Measure('dx', domain = mesh, subdomain_data = domains)
            n = FacetNormal(mesh)

            # normal force
            t_n = Constant(1.0)*n 
           
            # Variational problem
            #a1 = (lamb*inner(div(u),div(v)) + mu*inner(grad(u),grad(v))- alpha*inner(p,div(v)))*(dx(1)+dx(0))
            a1 = (inner(sigma(u),eps(v)) - alpha*inner(p,div(v)))*(dx(1)+dx(0))  
            L1 = -inner(f,v)*(dx(1)+dx(0)) - inner(t_n,v)*ds(4)
            a2 = (-c*inner(p,q) - alpha*inner(div(u),q))* (dx(1)+dx(0)) - K*dt*inner(grad(p),grad(q))*dx(0) - K_0*dt*inner(grad(p),grad(q))*dx(1)
            L2 = -(dt*inner(g,q) + alpha*inner(div(u0),q) + c*inner(p0,q))*(dx(1)+dx(0))
            a = a1 + a2 
            L = L1 + L2 
            
            A1 = assemble(a)
            [bc_.apply(A1) for bc_ in bc]


            # Numerical solution
            up_ = Function(W)

            # Solve for each time step t
            
            t = 0
            
            while t <= T:  
                # Update current time
                t += dt  

                b1 = assemble(L)
                [bc_.apply(b1) for bc_ in bc]

                # Solve variational problem
                solve(A1, up_.vector(), b1) 

                # Update previous solution 
                up0.assign(up_)
                 
                

            # Numerical solutions
            u_, p_ = up_.split()
        epsilon = 1E-8
        z = np.linspace(0, 1-epsilon, 101)
        points  = [(0,z_) for z_ in z]
        p_line = np.array([p_(point) for point in points])
        visualize(z, p_line, deg)

    plt.show()
           
            
    ufile = File('res/u.pvd')
    pfile = File('res/p.pvd')
    ufile << u_
    pfile << p_

def visualize(z, p_line, deg):  

    plt.plot(z, p_line, label ="degree=%.d"%deg) #, z, sigma_line, 'b--')
    plt.hold(True)
    plt.xlabel("$z$", fontsize=20)
    plt.ylabel("$pressure$", fontsize=20)  
    plt.legend(loc='upper left', frameon=False)

biot()
