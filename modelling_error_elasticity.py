from dolfin import *
import numpy as np
import matplotlib.pyplot as plt


set_log_active(False)

# Define Dirichlet boundary 
def boundary(x):
    return abs(x[0])< DOLFIN_EPS 


# Create mesh and define function space
n = 64

mesh = UnitSquareMesh(n, n)

V = VectorFunctionSpace(mesh, "Lagrange", 2)
DG0 = FunctionSpace(mesh, 'DG',0)

# Define boundary conditions and IC
u_ = Constant((0.0,0.0)) # primal u
u_0 = Constant((0.0,0.0)) # coarse u0
w_ = Constant((0.0,0.0))
w_0 = Constant((0.0,0.0))

bc_u = DirichletBC(V, u_, boundary)    # primal u
bc_u0 = DirichletBC(V, u_0, boundary)  # coarse u0
bc_w = DirichletBC(V, w_, boundary)    # primal dual w
bc_w0 = DirichletBC(V, w_0, boundary)     # coarse dual w0


# Define variational problem
u = TrialFunction(V)  # primal u
u0 = TrialFunction(V)  # coarse u
w = TrialFunction(V)# primal dual w
w0 = TrialFunction(V)  # coarse dual w
v = TestFunction(V)
f = Constant((0.0,1.0))

# Elasticity parameters

mu = Function(DG0)
K = 1.0

n = DG0.dim()

for i in range(n):
    np.random.seed(n)
    mu.vector()[i] = 1.0 + 0.01*np.random.rand(n)[i]


# Compute e.g. average to define mu0
mu0 = Constant(1.0/n*sum(mu.vector().array()))  

lamb = Constant(100.0)
lamb_0 = lamb

# Elasticity tensor
def eps(v):
    return 0.5*(grad(v) + grad(v).T)

def E_eps_u(v):
    return 2*mu*eps(v) + lamb*tr(eps(v))*Identity(2)

def E0_eps_u(v):
    return 2*mu0*eps(v) + lamb_0*tr(eps(v))*Identity(2)

# solve coarse problem 
a_u0 = inner(E0_eps_u(u0),eps(v))*dx 
L = inner(v,f)*dx 

# solve primal problem 
a_u = inner(E_eps_u(u),eps(v))*dx 

# solve dual coarse problem
a_w0 = inner(E0_eps_u(w0),eps(v))*dx
L_w = inner(v,Constant((1.0,1.0)))*dx

# solve dual primal problem 
a_w = inner(E_eps_u(w),eps(v))*dx

# Define function for the solutions
u = Function(V) 
u0 = Function(V) 
w = Function(V)
w0 = Function(V)

solve(a_w0 == L_w, w0, bc_w0)
solve(a_u0 == L, u0, bc_u0)
solve(a_u == L, u, bc_u)
solve(a_w == L_w, w, bc_w)


def Einv(A):
    return 1./(2*mu)*(A - lamb/(2*mu + 2*lamb)*tr(A)*Identity(2))

def EinvE0_eps(v):
    return Einv(E0_eps_u(v))

def EI0_eps(v):
    return E_eps_u(v)*Identity(2) - E0_eps_u(v)

def Modeling_Residual(v,u0):
    return (assemble(-inner(eps(v),EI0_eps(u0))*dx))    

def Ksi_upp(v):
    return np.sqrt(assemble(inner(Identity(2)*eps(v) - EinvE0_eps(v),EI0_eps(v))*dx))

def Ksi_low(v):
    return (np.abs(Modeling_Residual(v,v))/norm(v))

def S():
    return np.sqrt(Ksi_upp(w0)/Ksi_upp(u0))

def mellom():
    return np.sqrt(assemble((inner(Identity(2)*eps(u0) - EinvE0_eps(u0),EI0_eps(w0))*dx)))

def Eta_upp_plus():
    mellom = (assemble((inner(Identity(2)*eps(u0) - EinvE0_eps(u0),EI0_eps(w0))*dx)))
    return np.sqrt((S()**2 * Ksi_upp(u0)**2 + 2.*mellom + S()**(-2.)*Ksi_upp(w0)**2))

def Eta_upp_minus():
    mellom = (assemble((inner(Identity(2)*eps(u0) - EinvE0_eps(u0),EI0_eps(w0))*dx)))
    return np.sqrt(S()**2 * Ksi_upp(u0)**2 - 2.*mellom + S()**(-2.)*Ksi_upp(w0)**2)

def eta_est_upp(): 
    return 1./4.*(Eta_upp_plus()**2) - 1./4.*(Eta_upp_minus()**2) + Modeling_Residual(u0,w0)

def norm(v):
    return np.sqrt(assemble((inner(eps(v),E_eps_u(v))*dx)))

def theta_plus():
    B_u_w = assemble(inner(eps(u0), E_eps_u(w0))*dx)
    B_u_u = assemble(inner(eps(u0), E_eps_u(u0))*dx)
    B_w_w = assemble(inner(eps(w0), E_eps_u(w0))*dx)
    res_u = assemble((-inner((S()*eps(u0) + 1./S() *eps(w0)), E_eps_u(u0) -E0_eps_u(u0)) )*dx)
    res_w = assemble((-inner((S()*eps(u0) + 1./S() *eps(w0)), E_eps_u(w0) -E0_eps_u(w0)) )*dx)
    return (B_u_w*res_u - B_u_u*res_w)/(B_u_w*res_w - B_w_w*res_u)

def theta_minus():
    B_u_w = assemble(inner(eps(u0), E_eps_u(w0))*dx)
    B_u_u = assemble(inner(eps(u0), E_eps_u(u0))*dx)
    B_w_w = assemble(inner(eps(w0), E_eps_u(w0))*dx)
    res_u = assemble((-inner((S()*eps(u0) - 1./S() *eps(w0)), EI0_eps(u0)) )*dx)
    res_w = assemble((-inner((S()*eps(u0) - 1./S() *eps(w0)), EI0_eps(w0)) )*dx)
    return (B_u_w*res_u - B_u_u*res_w)/(B_u_w*res_w - B_w_w*res_u)

def eta_low_plus():
    res_u_w = assemble(-inner((eps(u0) + theta_plus()*eps(w0)), (S()*EI0_eps(u0) + 1./S()*EI0_eps(w0)  ) )*dx )    
    norm_u_w = assemble((inner((eps(u0) + theta_plus()*eps(w0)), E_eps_u(u0) + theta_plus()*E_eps_u(w0)))*dx)
    eta_low_plus = np.abs(res_u_w)/(norm_u_w)
    return eta_low_plus

def eta_low_minus():
    res_u_w = assemble((-inner((eps(u0) + theta_minus()*eps(w0)), S()*EI0_eps(u0) - 1./S()*EI0_eps(w0)))*dx )    
    norm_u_w = np.sqrt(assemble((inner((eps(u0) + theta_minus()*eps(w0)), E_eps_u(u0) + theta_minus()*E_eps_u(w0)))*dx))
    eta_low_minus = np.abs(res_u_w)/(norm_u_w)
    return eta_low_minus

def eta_low():
    return 1./4 * (eta_low_minus()**2) -1./4*(Eta_upp_minus()**2) + Modeling_Residual(w0,u0) 

def eta_upp():
    return  1./4 * (Eta_upp_plus()**2) -1./4*(eta_low_minus()**2) + Modeling_Residual(w0,u0)  

def norm_e0_plus():
    grad_s_e0 = S()*eps(u-u0) + 1./S()*eps(w-w0)
    E_grad = S()*E_eps_u(u-u0) +1./S()*E_eps_u(w-w0)
    return np.sqrt(assemble(inner(grad_s_e0,E_grad)*dx))

def norm_e0_minus():
    grad_s_e0 = S()*eps(u-u0) - 1./S()*eps(w-w0)
    E_grad = S()*E_eps_u(u-u0) - 1./S()*E_eps_u(w-w0)
    return np.sqrt(assemble(inner(grad_s_e0,E_grad)*dx))

mellom_ = mellom()
eta_low_plus_= eta_low_plus()
eta_low_minus_= eta_low_minus()
eta_low_= eta_low()
eta_upp_= eta_upp()    
ksiU = Ksi_upp(u0)
ksiW = Ksi_upp(w0)
ksiu = Ksi_low(u0)
ksiw = Ksi_low(w0)
eta_p = Eta_upp_plus()
eta_m = Eta_upp_minus()
error_u = norm(u-u0)
error_w = norm(w-w0)
s = S()
eta_esttt = eta_est_upp() 
u_effectivity_ind_upp = ksiU/error_u
u_effectivity_ind_low = ksiu/error_u
w_effectivity_ind_upp = ksiW/error_w
w_effectivity_ind_low = ksiw/error_w
L_e0 = Modeling_Residual(w,u0)
norm_e0_plus_ = norm_e0_plus()
norm_e0_minus_ = norm_e0_minus()

lamb_low_plus = eta_low_plus_/norm_e0_plus_
lamb_low_minus = eta_low_minus_/norm_e0_minus_

lamb_upp_plus = eta_p/norm_e0_plus_
lamb_upp_minus = eta_m/norm_e0_minus_
gamma_low = eta_low_/eta_esttt
gamma_upp = eta_upp_/eta_esttt
#print ('lamb_low_plus = %.2e, lamb_low_minus = %.2e  '% (lamb_low_plus, lamb_low_minus))
#print ('lamb_upp_plus = %.2e, lamb_upp_minus = %.2e  '% (lamb_upp_plus, lamb_upp_minus))

if ksiu<= error_u and error_u <= ksiU:
    print ("ksi_low_u = %.2e, error_u = %.2e, ksi_upp_u = %.2e" %(ksiu, error_u, ksiU))
    print ('u_effectivity_ind_upp = %.2e, u_effectivity_ind_low = %.2e' % (u_effectivity_ind_upp,u_effectivity_ind_low))
else:
    print ('Test is not pass') 

if ksiw<= error_w and error_w <= ksiW:
    print ("ksi_low_w = %.2e, error_w = %.2e, ksi_upp_w = %.2e" %(ksiw, error_w, ksiW))
    print ('w_effectivity_ind_upp = %.2e, w_effectivity_ind_low = %.2e' % (w_effectivity_ind_upp,w_effectivity_ind_low))
else:
    print ('Test is not pass')     

if eta_low_ <= eta_esttt and eta_esttt <= eta_upp_ :
    print ('eta_low = %.2e, L(e) = %.2e, eta_upp = %.2e' % (eta_low_,eta_esttt,eta_upp_))
    print ('gamma_low = %.2e, gamma_upp = %.2e' % (gamma_low,gamma_upp))
else:
    print ('Test is not pass')

error = error_w/error_u
print ('error_u_w = %.2e' % error)


ufile = File('res/u_0.pvd')
ufile << u0
ufile = File('res/w_0.pvd')
ufile << w0
file = File("res/mu.pvd")
file << mu
file = File("res/w.pvd")
file << w
file = File("res/u.pvd")
file << u