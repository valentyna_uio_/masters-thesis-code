from dolfin import *
import numpy as np
import matplotlib.pyplot as plt
from modelling_error_pressure import modelling_error_p


set_log_active(False)

# Define Dirichlet boundary 
def u_boundary(x):
    return x[0] < DOLFIN_EPS or x[1] > 1.0 - DOLFIN_EPS 

p, p0, w, w0, K, K0, ksi_upp_w, ksi_low_w, ksi_upp_p, ksi_low_p, gamma_low_minus_, gamma_low_plus_, gamma_upp_minus_, gamma_upp_plus_, mesh, n, error_p = modelling_error_p()

V = VectorFunctionSpace(mesh, "Lagrange", 2)
DG0 = FunctionSpace(mesh, 'DG',0)

# Define boundary conditions and IC
u_ = Constant((0.0,0.0)) # primal u
u_0 = Constant((0.0,0.0)) # coarse u0
w_u = Constant((0.0,0.0))
w_u0 = Constant((0.0,0.0))

bc_u = DirichletBC(V, u_, u_boundary)    # primal u
bc_u0 = DirichletBC(V, u_0, u_boundary)  # coarse u0
bc_wu = DirichletBC(V, w_u, u_boundary)    # primal dual w
bc_wu0 = DirichletBC(V, w_u0, u_boundary)     # coarse dual w0


# Define variational problem
u = TrialFunction(V)  # primal u
u0 = TrialFunction(V)  # coarse u
wu = TrialFunction(V)# primal dual w
wu0 = TrialFunction(V)  # coarse dual w
v = TestFunction(V)
f = Constant((0.0,1.0))

# Elasticity parameters
mu = Function(DG0)
n = DG0.dim()

for i in range(n):
    np.random.seed(n)
    mu.vector()[i] = 1.0 + 0.5*np.random.rand(n)[i]

# Compute e.g. average to define mu0
mu0 = Constant(1.0/n*sum(mu.vector().array()))  
lamb = Constant(100.0)
lamb_0 = lamb

# Elasticity tensor
def eps(v):
    return 0.5*(grad(v) + grad(v).T)

def E_eps_u(v):
    return 2*mu*eps(v) + lamb*tr(eps(v))*Identity(2)

def E0_eps_u(v):
    return 2*mu0*eps(v) + lamb_0*tr(eps(v))*Identity(2)

# solve coarse problem 
a_u0 = inner(E0_eps_u(u0),eps(v))*dx 
L0 = inner(v,f)*dx + (div(v)*p0)*dx

# solve primal problem 
a_u = inner(E_eps_u(u),eps(v))*dx 
L = inner(v,f)*dx +(div(v)*p)*dx


# solve dual coarse problem
a_w0 = inner(E0_eps_u(wu0),eps(v))*dx 
L_w = inner(v,Constant((1.0,1.0)))*dx #+(div(v)*w0)*dx

# solve dual primal problem 
a_wu = inner(E_eps_u(wu),eps(v))*dx 
# Define function for the solutions
u = Function(V) 
u0 = Function(V) 
wu = Function(V)
wu0 = Function(V)

solve(a_w0 == L_w, wu0, bc_wu0)
solve(a_u0 == L0, u0, bc_u0)
solve(a_u == L, u, bc_u)
solve(a_wu == L_w, wu, bc_wu)


def Einv(A):
    return 1./(2*mu)*(A - lamb/(2*mu + 2*lamb)*tr(A)*Identity(2))

def EinvE0_eps(v):
    return Einv(E0_eps_u(v))

def EI0_eps(v):
    return E_eps_u(v)*Identity(2) - E0_eps_u(v)

def Modelling_Residual(v,u0):
    return (assemble(-inner(eps(v),EI0_eps(u0))*dx))  

def residual_2():
    return assemble(-(dot(div(wu0),ksi_upp_p)*dx))     

def zeta_upp(v):
    return float(np.sqrt(assemble(inner(Identity(2)*eps(v) - EinvE0_eps(v),EI0_eps(v))*dx)) )

def zeta_low(v):
    return (np.abs(Modelling_Residual(v,v))/norm(v))

def S_u():
    return float(np.sqrt(zeta_upp(wu0)/zeta_upp(u0)))

def mellom():
    return float(np.sqrt(assemble((inner(Identity(2)*eps(u0) - EinvE0_eps(u0),EI0_eps(wu0))*dx))))

def eta_upp_plus_u():
    mellom = (assemble((inner(Identity(2)*eps(u0) - EinvE0_eps(u0),EI0_eps(wu0))*dx)))
    return float(np.sqrt((S_u()**2 * zeta_upp(u0)**2 + 2.*mellom + S_u()**(-2.)*zeta_upp(wu0)**2)))

def eta_upp_minus_u():
    mellom = (assemble((inner(Identity(2)*eps(u0) - EinvE0_eps(u0),EI0_eps(wu0))*dx)))
    return float(np.sqrt(S_u()**2 * zeta_upp(u0)**2 - 2.*mellom + S_u()**(-2.)*zeta_upp(wu0)**2))

def norm(v):
    return float(np.sqrt(assemble((inner(eps(v),E_eps_u(v))*dx))))

def theta_plus_u():
    B_u_w = assemble(inner(eps(u0), E_eps_u(wu0))*dx)
    B_u_u = assemble(inner(eps(u0), E_eps_u(u0))*dx)
    B_w_w = assemble(inner(eps(wu0), E_eps_u(wu0))*dx)
    res_u = assemble((-inner((S_u()*eps(u0) + 1./S_u() *eps(wu0)), E_eps_u(u0) -E0_eps_u(u0)) )*dx)
    res_ = assemble((-dot(div(S_u()*u0 + 1./S_u()*wu0), (p-p0))*dx))
    res_w = assemble((-inner((S_u()*eps(u0) + 1./S_u() *eps(wu0)), E_eps_u(wu0) -E0_eps_u(wu0)) )*dx)
    return (B_u_w*(res_u+res_) - B_u_u*(res_w+res_))/(B_u_w*(res_+res_w) - B_w_w*(res_+res_u))

def theta_minus_u():
    B_u_w = assemble(inner(eps(u0), E_eps_u(wu0))*dx)
    B_u_u = assemble(inner(eps(u0), E_eps_u(u0))*dx)
    B_w_w = assemble(inner(eps(wu0), E_eps_u(wu0))*dx)
    res_ = assemble((-dot(div(S_u()*u0 + 1./S_u()*wu0), (p-p0))*dx))
    res_u = assemble((-inner((S_u()*eps(u0) - 1./S_u() *eps(wu0)), EI0_eps(u0)) )*dx)
    res_w = assemble((-inner((S_u()*eps(u0) - 1./S_u() *eps(wu0)), EI0_eps(wu0)) )*dx)
    return (B_u_w*(res_+res_u) - B_u_u*(res_+res_w))/(B_u_w*(res_+res_w) - B_w_w*(res_+res_u))

def eta_low_plus_u():
    res_u_w = assemble(-inner((eps(u0) + theta_plus_u()*eps(wu0)), (S_u()*EI0_eps(u0) + 1./S_u()*EI0_eps(wu0)  ) )*dx )    
    norm_u_w = assemble((inner((eps(u0) + theta_plus_u()*eps(wu0)), E_eps_u(u0) + theta_plus_u()*E_eps_u(wu0)))*dx)
    eta_low_plus_u = float(np.abs(res_u_w)/(norm_u_w))
    return eta_low_plus_u

def eta_low_minus_u():
    res_u_w = assemble((-inner((eps(u0) + theta_minus_u()*eps(wu0)), S_u()*EI0_eps(u0) - 1./S_u()*EI0_eps(wu0)))*dx )    
    norm_u_w = np.sqrt(assemble((inner((eps(u0) + theta_minus_u()*eps(wu0)), E_eps_u(u0) + theta_minus_u()*E_eps_u(wu0)))*dx))
    eta_low_minus = float(np.abs(res_u_w)/(norm_u_w))
    return eta_low_minus

def eta_low():
    return 1./4 * (eta_low_minus_u()**2) -1./4*(eta_upp_minus_u()**2) + Modelling_Residual(wu0,u0) 

def eta_upp():
    return  1./4 * (eta_upp_plus_u()**2) -1./4*(eta_low_minus_u()**2) + Modelling_Residual(wu0,u0)  

def norm_e0_plus_u():
    grad_s_e0 = S_u()*eps(u-u0) + 1./S_u()*eps(wu-wu0)
    E_grad = S_u()*E_eps_u(u-u0) +1./S_u()*E_eps_u(wu-wu0)
    return np.sqrt(assemble(inner(grad_s_e0,E_grad)*dx))

def norm_e0_minus_u():
    grad_s_e0 = S_u()*eps(u-u0) - 1./S_u()*eps(wu-wu0)
    E_grad = S_u()*E_eps_u(u-u0) - 1./S_u()*E_eps_u(wu-wu0)
    return np.sqrt(assemble(inner(grad_s_e0,E_grad)*dx))

zeta_upp_u = zeta_upp(u0) + ksi_upp_p
zeta_upp_w = zeta_upp(wu0) 
zeta_low_u = zeta_low(u0) + ksi_low_p
zeta_low_w = zeta_low(wu0) 

error_u = norm(u-u0)
error_w = norm(wu-wu0)

u_effectivity_ind_upp = zeta_upp_u/error_u
u_effectivity_ind_low = zeta_low_u/error_u
w_effectivity_ind_upp = zeta_upp_w/error_w
w_effectivity_ind_low = zeta_low_w/error_w

eta_low_plus_u_= eta_low_plus_u()  + ksi_low_p
eta_low_minus_u_= eta_low_minus_u() + ksi_low_p
eta_upp_plus_u_ = eta_upp_plus_u() + ksi_upp_p
eta_upp_minus_u_ = eta_upp_minus_u() + ksi_upp_p

residual_2_ = residual_2()

eta_upp_ = 1./4*(eta_upp_plus_u_)**2 - 1./4*(eta_low_minus_u_)**2 + Modelling_Residual(u0,wu0) + residual_2_
eta_low_ = 1./4*(eta_low_plus_u_)**2 - 1./4*(eta_upp_minus_u_)**2 + Modelling_Residual(u0,wu0) + residual_2_


s = S_u()
eta_est_upp_ = 1./4*(eta_upp_plus_u_)**2 - 1./4*(eta_upp_minus_u_)**2 + Modelling_Residual(u0,wu0) + residual_2_

norm_e0_plus_ = norm_e0_plus_u()
norm_e0_minus_ = norm_e0_minus_u()

eta_effectivity_ind_low = eta_low_/eta_est_upp_
eta_effectivity_ind_upp = eta_upp_/eta_est_upp_

print ("---------------------DISPLACEMENT---------------------------------------")

if zeta_low_u<= error_u and error_u <= zeta_upp_u:
    print ("zeta_low_u = %.2e, error_u = %.2e, zeta_upp_uu = %.2e" %(zeta_low_u, error_u, zeta_upp_u))
    print ('u_effectivity_ind_low = %.2e, u_effectivity_ind_upp = %.2e' % (u_effectivity_ind_low,u_effectivity_ind_upp))
else:
    print ('Test is not pass') 

if zeta_low_w<= error_w and error_w <= zeta_upp_w:
    print ("------------------------------------------------------------")
    print ("zeta_low_w = %.2e, error_w = %.2e, zeta_upp_uw = %.2e" %(zeta_low_w, error_w, zeta_upp_w))
    print ('w_effectivity_ind_low = %.2e, w_effectivity_ind_upp = %.2e' % (w_effectivity_ind_low,w_effectivity_ind_upp))
else:
    print ('Test is not pass')     

if eta_low_ <= eta_est_upp_ and eta_est_upp_ <= eta_upp_ :
    print ("------------------------------------------------------------")
    print ('eta_low = %.2e, L(e) = %.2e, eta_upp = %.2e' % (eta_low_,eta_est_upp_,eta_upp_))
    print ('eta_effectivity_ind_low = %.2e, eta_effectivity_ind_upp = %.2e' % (eta_effectivity_ind_low,eta_effectivity_ind_upp))
else:
    print ('Test is not pass')

